Graphcore vPOD Terraform Example
================================

Within this repo is an example of using Terraform to configure Openstack resources to support the use of IPUMs inside a virtual-POD (vPOD).   The example configuration includes two parts, one to configure a set of common infrastructure (used to access IPUM gateways and BMCs), and the other is used to deploy dedicated infrastructure for a vPOD. 

The Graphcore Openstack cloud has been deployed by Kayobe, but this configuration should be compatible most deployments.   The primary technical requirements for this configuration are:
* Use of SR-IOV and HW accelerated OpenVSwitch (when using virtualised server instances)
* VLAN based provider networks.

To ensure that any VM instances are located on hypervisors on the same switch as the IPUMs they will be accessing, an availability zone is created for each POD64.  This allows a vPOD instance to be assigned to a particular AZ, and adjacent to the IPUMs. 

Common Infrastructure
---------------------
For this deployment, all gateway and BMC ports on each IPUM are connected to a common VLAN network.   This allows for out of band access to IPUMs for power management and firmware updates.   No customer resources are made available on this network. 

Once the network has been created, Neutron ports are created using the MAC addresses of each BMC and gateway port.   These ports will not be connected to any Openstack managed resources, but will provide DHCP provided IP addresses to each IPUM port.

vPOD Infrastructure
-------------------
A vPOD contains:
* vPOD dedicated networks:
    * general access  (10.3.X.0/24)
    * RNIC (RDMA) network (10.5.X.0/16)
    * Storage access netork (10.12.X.0/24)
* An router, connecting the general access network to an external network.
* a vpod-ctrl instance
* one (or more) Poplar instances (user facing systems)

Example Terraform is included in 'lr8-1/lr8-1.main.tf'.   The Terraform makes heavy use of Terraform modules, allowing for easy code re-use.    Alongside the 'main.tf' file is an 'auto.tfvars' file, including responses to Terraform variables used in 'main.tf'.  From the example lr8-1/lr8-1.auto.tfvars:

    vpod_name         = "lr8-1"
    ctrl_vlan         = 1234
    rnic_vlan         = 2345
    storage_vlan      = 3456
    cidr_hint         = 8
    vpod_floatingip   = "1.2.3.4"
    poplaraz          = "pod8"
    ipum_csv          = "rnic_pod8a.csv"
    assigned_ipums    = ["lr8-ipum1", "lr8-ipum2", "lr8-ipum3", "lr8-ipum4"]
    flavor            = "r6525.full_nps4"
    image             = "ubuntu-18.04-mlnx-v1_6"

* vpod_name: will be prepended onto the name of all the resources created
* ctrl_vlan: the VLAN ID for the provider network created for the general access network
* rnic_vlan: the VLAN ID for the provider network created for the RDMA connectivity to the IPUMs
* storage_vlan: the VLAN ID for the provider network created for storage access
* cidr_hint: the number to be used for the 3rd octet of the networks created in this vPOD
* vpod_floatingip: the floating IP to be attached (should already be assigned to the project)
* poplaraz: the availability zone for the vPOD
* ipum_csv: the name of the file to reference the IPUM RNIC MAC addresses and IP addresses.
* assigned_ipums: a list of the IPUMs assigned to the vPOD
* flavor: the Openstack flavor used to size the Poplar instance
* image: the Glance image to boot the Poplar instance and vpod-ctrl instance from

Networks
========
IP Addressing
-------------
For all of the IPUMs, we use Graphcore's 'global addressing' schema.   This uses the subnets:
BMC: 10.1.X.Y/16
GW: 10.2.X.Y/16
RNIC: 10.5.X.Y/16

where X is the rack/pod64, and Y is the IPUM position within the pod64.  For example, an IPUM in logical rack 7, position 6 would have:
BMC: 10.1.7.6
GW: 10.2.7.6
RNIC: 10.5.7.6

The use of pseudo-static IPs and /16 subnets means that IPUMs do not need IP addresses to be changed when moving between vPODs.  Instead, only the RNIC switchport VLAN needs to be changed.

IPUM RNIC Connections
---------------------
The management of an IPUM RNIC switchport VLAN is not included within this configuration.   IPUMs are outside of the direct control of Openstack, and so delegated switch management through Neutron (via ML2 or other plugins) is not possible.   

The Graphcore Openstack cloud uses Kayobe's network switch management tooling to move IPUMs between VLANs.  This means that although it is automated, the configuration is stored in two places.   Planned work to improve IPUM+Openstack integration will remove this limitation.

Development and Feedback
========================
The development of this tooling is ongoing, and any feedback is appreciated.  If you find any errors or bugs, please create an issue on the appropriate repository, or contact your Graphcore representative.

Thanks to StackHPC for their support with the initial development of this configuration. 
