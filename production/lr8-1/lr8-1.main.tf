terraform {
  required_version = ">= 1.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.43"
    }
    local = {
      version = "~> 2.1"
    }
  }
}

# Make use of gitlab state
terraform {
  backend "http" {
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }
}

variable "vpod_name" {
  type = string
}

variable "ctrl_vlan" {
  type = number
}

variable "rnic_vlan" {
  type = number
}

variable "storage_vlan" {
  type = number
}

variable "cidr_hint" {
  type = number
}

variable "ipum_csv" {
  type = string
}

variable "vpod_floatingip" {
  type = string  
}

variable "poplaraz" {
  type = string  
}

variable "assigned_ipums" {
  type = list(string)
}

variable "flavor" {
  type = string
  default = "r6525.full"
}

variable "storage_net_mtu" {
  type = number
  default = 1500
}

variable "image" {
  type    = string
}


#
# Create vpod networks
#
module "networks" {
  source            = "git::ssh://git@gitlab.com/gc-openstack/terraform-modules/vpod-nets.git"
  vpod_name         = var.vpod_name
  ctrl_vlan         = var.ctrl_vlan
  rnic_vlan         = var.rnic_vlan
  storage_vlan      = var.storage_vlan
  cidr_hint         = var.cidr_hint
  storage_net_mtu   = var.storage_net_mtu
}

#
# Add ipu machines into vpod network
#
module "ipum-rnics-8a" {
  source     = "git::ssh://git@gitlab.com/gc-openstack/terraform-modules/vpod-ipum-rnic-ports.git"
  vpod_name  = var.vpod_name
  csv        = var.ipum_csv
  network_id = module.networks.rnic_network_id
  subnet_id  = module.networks.rnic_subnet_id
}


#
# Create vpod controller
#
module "vpod-ctrl" {
  source             = "git::ssh://git@gitlab.com/gc-openstack/terraform-modules/vpod-ctrl.git"
  vpod_name          = var.vpod_name
  ctrl_network_id    = module.networks.ctrl_network_id
  ctrl_subnet_id     = module.networks.ctrl_subnet_id
  rnic_network_id    = module.networks.rnic_network_id
  rnic_subnet_id     = module.networks.rnic_subnet_id
  storage_network_id = module.networks.storage_network_id
  storage_subnet_id  = module.networks.storage_subnet_id
  assigned_ipums     = var.assigned_ipums
  cidr_hint          = var.cidr_hint
  image              = var.image
}

#
# Create poplar vm
#
module "vpod-poplar-1" {
  source             = "git::ssh://git@gitlab.com/gc-openstack/terraform-modules/vpod-poplar-vm.git"
  name_suffix        = "1" 
  vpod_name          = var.vpod_name
  ctrl_network_id    = module.networks.ctrl_network_id
  ctrl_subnet_id     = module.networks.ctrl_subnet_id
  rnic_network_id    = module.networks.rnic_network_id
  rnic_subnet_id     = module.networks.rnic_subnet_id
  storage_network_id = module.networks.storage_network_id
  storage_subnet_id  = module.networks.storage_subnet_id
  vpod_common_sg_id  = module.networks.vpod_internal_sg 
  vpod_floatingip    = var.vpod_floatingip
  poplaraz           = var.poplaraz
  flavor             = var.flavor
  cidr_hint          = var.cidr_hint
  image              = var.image
}

#
# attach a floating IP to the Poplar host
#
resource "openstack_networking_floatingip_associate_v2" "popfloatip-assoc" {
  floating_ip = var.vpod_floatingip
  port_id     = module.vpod-poplar-1.ctrl_port_id
}
