vpod_name         = "lr8-1"
ctrl_vlan         = 1234
rnic_vlan         = 2345
storage_vlan      = 3456
cidr_hint         = 8
vpod_floatingip   = "1.2.3.4"
poplaraz          = "lr8"
ipum_csv          = "rnic_pod8a.csv"
assigned_ipums    = ["lr8-ipum1", "lr8-ipum2", "lr8-ipum3", "lr8-ipum4"]
flavor = "r6525.full_nps4"
image = "ubuntu-18.04-mlnx-v1_6"

