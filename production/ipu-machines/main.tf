terraform {
  required_version = ">= 1.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.43"
    }
    local = {
      version = "~> 2.1"
    }
  }
}

# Create all the common networks
module "network" {
  source    = "git::ssh://git@gitlab.com/gc-openstack/terraform-modules/ipum-1g-net.git"
  ipum_vlan = 82
}

# Create the BMC and Gateway ports for rack 8
module "pod8" {
  source        = "git::ssh://git@gitlab.com/gc-openstack/terraform-modules/ipum-pod64-ports"
  ipum_pod_name = "pod8"
  network_id    = module.network.network_id
  bmc_subnet_id = module.network.bmc_subnet_id
  gw_subnet_id  = module.network.gw_subnet_id
  bmc_csv       = "bmc_pod8.csv"
  gw_csv        = "gw_pod8.csv"
}